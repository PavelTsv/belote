package table;

import player.utility.Pair;
import player.calls.Contract;
import player.deck.Card;
import player.deck.Suit;
import player.Player;
import player.Team;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class PlayTable {
    private final Contract dominantContract;
    private final List<Pair<Player, Card>> trickCards = new ArrayList<>();
    private final Map<Team, Integer> teamScores = new HashMap<>();
    private final Map<Team, List<Card>> teamCardPiles = new HashMap<>();
    private final Set<Team> teams;

    public PlayTable(final Contract dominantContract, final Set<Team> teams) {

        this.dominantContract = dominantContract;
        this.teams = teams;
        teams.forEach(team -> {
            teamScores.put(team, 0);
            teamCardPiles.put(team, new ArrayList<>());
        });
    }

    public List<Card> getCardPile() {
        final List<Card> cardPile = new ArrayList<>();

        teams.forEach(team -> {
            cardPile.addAll(teamCardPiles.get(team));
            teamCardPiles.get(team).clear();
        });
        return cardPile;
    }

    public int getTeamScore(final Team team) {
        return teamScores.get(team);
    }

    public List<Card> getTrickCards() {
        return trickCards.stream()
                .map(playerCardPair -> playerCardPair.second)
                .collect(Collectors.toList());
    }

    public Player getTrickWinner(final Suit dominantSuit) {
        if (!(dominantContract.equals(Contract.ALL_TRUMPS)
                || dominantContract.equals(Contract.NO_TRUMPS))) {

            final Suit contractSuit = Suit.valueOf(dominantContract.name());
            final Optional<Pair<Player, Card>> strongestCardFromContractSuit =
                    getStrongestCardFrom(contractSuit);

            if (strongestCardFromContractSuit.isPresent()) {
                return strongestCardFromContractSuit.get().first;
            }
        }
        return getStrongestCardFrom(dominantSuit).get().first;
    }

    public void addCardToTrick(final Player player, final Card card) {
        trickCards.add(new Pair<>(player, card));
    }

    public void addScoreTo(final Team team, final int points) {
        final int teamPoints = teamScores.get(team) + points;
        teamScores.put(team, teamPoints);
    }

    public void addCardsToWinningTeamPile(final Team winningTeam) {
        teamCardPiles.get(winningTeam).addAll(trickCards.stream()
                .map(playerCardPair -> playerCardPair.second)
                .collect(Collectors.toList()));
        final int score = trickCards.stream()
                .map(playerCardPair -> playerCardPair.second)
                .mapToInt(card -> card.getValue(dominantContract))
                .sum();
        final Integer scoreSoFar = teamScores.get(winningTeam) + score;
        trickCards.clear();
        teamScores.put(winningTeam, scoreSoFar);
    }

    public void calculateFinalScore(final Team teamCalled) {
        if (dominantContract.equals(Contract.NO_TRUMPS)) {
            teams.forEach(team -> team.addScore(team.getScore()));
        }

        if (teamScores.get(teamCalled) < teams.stream().min(Comparator
                .comparing(Team::getScore)).get().getScore()) {
            teams.forEach(team -> {
                if (!team.equals(teamCalled)) {
                    team.addScore(teamScores.get(teamCalled));
                }
            });
            teamScores.put(teamCalled, 0);
        }
        teamScores.forEach((team, score) -> {
            final float endOfRoundDivider = 10.0f;
            teamScores.put(team, Math.round(score / endOfRoundDivider));
        });
    }

    private Optional<Pair<Player, Card>> getStrongestCardFrom(final Suit suit) {
        return trickCards.stream()
                .filter(playerCardPair -> playerCardPair.second.getSuit()
                        .equals(suit))
                .max(Comparator.comparing(playerCardPair ->
                        playerCardPair.second.getValue(dominantContract)));
    }
}
