import player.ComputerPlayer;
import player.Player;
import player.HumanPlayer;
import player.Team;
import player.utility.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Belote {

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Player player1 = new HumanPlayer("Pavel", scanner);
        final Player player2 = new ComputerPlayer("Gosho");
        final Player player3 = new ComputerPlayer("Georgi");
        final Player player4 = new ComputerPlayer("Ivan");
        final Game game = new Game(player1, player2, player3, player4);

        final List<String> acceptedAnswers = new ArrayList<>();
        acceptedAnswers.add("yes");
        acceptedAnswers.add("y");
        acceptedAnswers.add("n");
        acceptedAnswers.add("no");
        String answer;

        System.out.println(Message.get(Message.START_GAME));
        do {
            answer = scanner.nextLine();
            answer = answer.toLowerCase().trim();
        }
        while (!acceptedAnswers.contains(answer));
        if (answer.equals("no") || answer.equals("n")) {
            System.exit(0);
        } else {
            System.out.println();
            System.out.println(Message.get(Message.LETS_PLAY));
            System.out.println();
            game.start();
        }
    }
}
