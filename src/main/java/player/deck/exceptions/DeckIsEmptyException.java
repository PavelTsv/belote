package player.deck.exceptions;

public class DeckIsEmptyException extends Exception {
    public DeckIsEmptyException() {
    }

    public DeckIsEmptyException(final String message) {
        super(message);
    }

    public DeckIsEmptyException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
