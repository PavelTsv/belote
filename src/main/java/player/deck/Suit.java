package player.deck;

public enum Suit {
    CLUBS, DIAMONDS, HEARTS, SPADES
}
