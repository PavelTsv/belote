package player.deck;

import player.deck.exceptions.DeckIsEmptyException;

import java.util.Deque;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public final class Deck {
    private static final String CARDS_RESOURCE = "cards.CardsBundle";

    private final Deque<Card> deck = new LinkedList<>();

    public Deck() {
        final ResourceBundle cardsBundle =
                ResourceBundle.getBundle(CARDS_RESOURCE);
        fillDeckWithCardsFrom(cardsBundle);
    }

    public Card drawCard() throws DeckIsEmptyException {
        if (deck.isEmpty()) {
            throw new DeckIsEmptyException(
                    "Illegal attempt to draw card when deck is empty");
        }
        return deck.removeLast();
    }

    public void addCards(final List<Card> cards) {
        deck.addAll(cards);
    }

    public void cutAt(final int index) {
        if (index <= 0 || index >= deck.size()) {
            throw new IndexOutOfBoundsException("Illegal deck cut index");
        }
        for (int i = 0; i < index; i++) {
            deck.addLast(deck.removeFirst());
        }
    }

    public int getSize() {
        return deck.size();
    }

    private void fillDeckWithCardsFrom(final ResourceBundle cards) {
        final int SUIT_INDEX = 0;
        final int RANK_INDEX = 1;
        final int REPRESENTATION_INDEX = 2;
        final int NO_TRUMP_INDEX = 3;
        final int ALL_TRUMP_INDEX = 4;

        final Enumeration<String> cardKeys = cards.getKeys();

        while (cardKeys.hasMoreElements()) {
            final String key = cardKeys.nextElement();
            final String[] cardInfo = cards.getString(key).split(" ");
            final int[] codePoints = new int[1];
            codePoints[0] = Integer.decode(cardInfo[REPRESENTATION_INDEX]);
            final int offset = 0;
            final int count = 1;
            final String cardStringRepresentation =
                    new String(codePoints, offset, count);
            final int noTrumpValue =
                    Integer.parseInt(cardInfo[NO_TRUMP_INDEX]);
            final int trumpValue =
                    Integer.parseInt(cardInfo[ALL_TRUMP_INDEX]);
            final Card card = new Card(
                    Suit.valueOf(cardInfo[SUIT_INDEX]),
                    Rank.valueOf(cardInfo[RANK_INDEX]),
                    cardStringRepresentation, noTrumpValue, trumpValue);
            deck.addLast(card);
        }

    }
}
