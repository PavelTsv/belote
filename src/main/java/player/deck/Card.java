package player.deck;

import player.calls.Contract;

import java.util.Objects;

public final class Card {
    private final Suit suit;
    private final Rank rank;
    private final String representation;
    private final int noTrumpValue;
    private final int trumpValue;

    Card(final Suit suit, final Rank rank, final String representation,
         final int noTrumpValue, final int trumpValue) {

        this.suit = suit;
        this.rank = rank;
        this.representation = representation;
        this.noTrumpValue = noTrumpValue;
        this.trumpValue = trumpValue;
    }

    public Suit getSuit() {
        return suit;
    }

    public Rank getRank() {
        return rank;
    }

    public int getValue(final Contract contract) {
        if (contract.name().equals(suit.name())
                || contract.equals(Contract.ALL_TRUMPS)) {
            return trumpValue;
        }
        return noTrumpValue;
    }

    @Override
    public String toString() {
        return representation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return noTrumpValue == card.noTrumpValue &&
                trumpValue == card.trumpValue &&
                suit == card.suit &&
                rank == card.rank &&
                representation.equals(card.representation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(suit, rank, representation, noTrumpValue,
                trumpValue);
    }
}
