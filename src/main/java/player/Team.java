package player;

import player.utility.Message;

import java.util.Objects;

public class Team {
    private int score = 0;
    private final String name;

    public Team(final String name) {
        this.name = name;
    }

    public void addScore(final int score) {
        this.score += score;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return Message.get(Message.TEAM) + " " + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return name.equals(team.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
