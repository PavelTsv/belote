package player;

import player.calls.Contract;
import player.deck.Card;
import player.deck.Suit;

import java.util.List;

public class ComputerPlayer extends Player {
    public ComputerPlayer(final String name) {
        super(name);
    }

    @Override
    public Contract bid(final Contract highestContract) {
        final List<Contract> availableBids = getValidContracts(highestContract);
        final int randomMove = (int) (Math.random() * availableBids.size());
        return availableBids.get(randomMove);
    }

    @Override
    public Card playCard(final Contract contract,
                         final List<Card> cardsPlayedSoFar,
                         final Suit dominantSuit) {
        final List<Card> validCards = getValidCards(contract,
                cardsPlayedSoFar, dominantSuit);
        final int randomCard = (int) (Math.random() * validCards.size());
        final Card card = validCards.get(randomCard);
        hand.remove(card);
        return card;
    }

    @Override
    public int chooseIndexToCutDeckAt(int start, int end) {
        return (int) (Math.random() * (end - start) + start);
    }
}
