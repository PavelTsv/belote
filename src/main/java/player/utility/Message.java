package player.utility;

import java.util.ResourceBundle;

public enum Message {
    YOUR_HAND("YourHandMessage"),
    VIABLE_CONTRACT("ViableContractMessage"),
    CHOOSE_CONTRACT("ChooseContractMessage"),
    CARDS_ON_TABLE("CardsOnTableMessage"),
    CARDS_YOU_CAN_PLAY("CardsYouCanPlayMessage"),
    CHOOSE_CARD("ChooseCardMessage"),
    CHOOSE_CUT_INDEX("ChooseCutIndexMessage"), TEAM("TeamMessage"),
    FIRST_TEAM_NAME("FirstTeamName"),
    SECOND_TEAM_NAME("SecondTeamName"),
    DEAL_CARDS("CardsDealtMessage"), BIDDING("BiddingMessage"),
    CURRENT_CONTRACT("CurrentContractMessage"),
    PLAYED("PlayedMessage"), LINE_SEPARATOR("LineSeparator"),
    PLAYER("Player"), ANNOUNCED("Announced"), BIDS("Bids"),
    WON_TRICK("WonTrickMessage"), WINNER("WinnerMessage"),
    ROUND("RoundMessage"), SCORE("ScoreMessage"),
    START_GAME("StartGameMessage"), LETS_PLAY("LetsPlayMessage");

    public static final String LANGUAGE_LANGUAGE_BUNDLE =
            "language.LanguageBundle";
    private static final ResourceBundle bundle =
            ResourceBundle.getBundle(LANGUAGE_LANGUAGE_BUNDLE);

    private final String name;

    Message(final String name) {
        this.name = name;
    }

    public static String get(final Message message) {
        return bundle.getString(message.toString());
    }

    @Override
    public String toString() {
        return name;
    }
}
