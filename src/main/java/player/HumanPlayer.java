package player;

import player.calls.Contract;
import player.deck.Card;
import player.deck.Suit;
import player.utility.Message;

import java.util.List;
import java.util.Scanner;

public class HumanPlayer extends Player {
    private final Scanner input;

    public HumanPlayer(final String name, final Scanner input) {
        super(name);
        this.input = input;
    }

    @Override
    public Contract bid(final Contract highestContract) {
        final String handToString = handToString();
        System.out.println(handToString);

        final List<Contract> viableBids = getValidContracts(highestContract);
        System.out.println(contractsToString(viableBids));
        final String message = Message.get(Message.CHOOSE_CONTRACT);
        System.out.printf(message, 0, viableBids.size() - 1);
        System.out.println();
        final int contract = input.nextInt();

        return viableBids.get(contract);
    }

    @Override
    public Card playCard(final Contract contract,
                         final List<Card> cardsPlayedSoFar,
                         final Suit dominantSuit) {
        System.out.println();
        final String cardsOnTable = Message.get(Message.CARDS_ON_TABLE);
        System.out.println(cardsOnTable);

        final String cardsPlayedSoFarToString =
                collectionToString(cardsPlayedSoFar);
        System.out.println(cardsPlayedSoFarToString);

        final String handToString = handToString();
        System.out.println(handToString);
        final List<Card> validCards =
                getValidCards(contract, cardsPlayedSoFar, dominantSuit);
        final String cardsToString = collectionToString(validCards);
        final String cardsYouCanPlay = Message.get(Message.CARDS_YOU_CAN_PLAY);
        System.out.println(cardsYouCanPlay);
        System.out.println(cardsToString);
        final String chooseCard = Message.get(Message.CHOOSE_CARD);
        System.out.printf(chooseCard, 0, validCards.size() - 1);
        System.out.println();
        final int cardIndex = input.nextInt();
        final Card card = validCards.get(cardIndex);
        hand.remove(card);
        return card;
    }

    @Override
    public int chooseIndexToCutDeckAt(final int start, final int end) {
        final String chooseCutIndex = Message.get(Message.CHOOSE_CUT_INDEX);
        System.out.printf(chooseCutIndex, start, end);
        System.out.println();
        final int index = input.nextInt();
        return index;
    }
}
