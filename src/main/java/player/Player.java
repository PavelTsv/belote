package player;

import player.calls.Contract;
import player.calls.Declaration;
import player.deck.Card;
import player.deck.Rank;
import player.deck.Suit;
import player.utility.Message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Player {
    final List<Card> hand = new ArrayList<>();
    private final String name;

    Player(final String name) {
        this.name = name;
    }

    public void addToHand(final Card card) {
        hand.add(card);
    }

    public List<Card> dumpHand() {
        final List<Card> handCopy = new ArrayList<>(hand);
        hand.clear();
        return handCopy;
    }

    public List<Declaration> makeDeclarations() {
        final List<Declaration> playerDeclarations = new ArrayList<>();

        final Collection<List<Card>> suitGroups = hand.stream()
                .collect(Collectors.groupingBy(Card::getSuit)).values();

        addSequenceDeclarations(playerDeclarations, suitGroups);

        final Collection<List<Card>> rankGroups = hand.stream()
                .collect(Collectors.groupingBy(Card::getRank)).values();

        addCarreDeclarations(playerDeclarations, rankGroups);

        if (playerDeclarations.isEmpty()) {
            playerDeclarations.add(Declaration.NONE);
        }
        return playerDeclarations;
    }

    private static void addCarreDeclarations(
            final List<Declaration> playerDeclarations,
            final Collection<List<Card>> rankGroups) {

        for (final List<Card> rankList : rankGroups) {
            final int cardsForSequenceRequired = 4;
            if (rankList.size() == cardsForSequenceRequired) {
                final Rank rank = rankList.get(0).getRank();
                switch (rank) {
                    case JACK:
                        playerDeclarations.add(Declaration.CARRE_OF_JACKS);
                        break;
                    case NINE:
                        playerDeclarations.add(Declaration.CARRE_OF_NINES);
                        break;
                    case ACE:
                        playerDeclarations.add(Declaration.CARRE_OF_ACES);
                        break;
                    case KING:
                        playerDeclarations.add(Declaration.CARRE_OF_KINGS);
                    case QUEEN:
                        playerDeclarations.add(Declaration.CARRE_OF_QUEENS);
                    case TEN:
                        playerDeclarations.add(Declaration.CARRE_OF_TENS);
                    default:
                        break;
                }
            }
        }
    }

    private static void addSequenceDeclarations(
            final List<Declaration> playerDeclarations,
            final Collection<List<Card>> suitGroups) {

        for (final List<Card> suitList : suitGroups) {
            suitList.sort(Comparator.comparing(Card::getRank));
            if (containsBelote(suitList)) {
                playerDeclarations.add(Declaration.BELOTE);
            }
            final int highestSequence = getHighestSequence(suitList);
            addDeclarations(playerDeclarations, highestSequence);
        }
    }

    private static void addDeclarations(
            final List<Declaration> playerDeclarations,
            final int highestSequence) {

        switch (highestSequence) {
            case 3:
                playerDeclarations.add(Declaration.TIERCE);
                break;
            case 4:
                playerDeclarations.add(Declaration.QUARTE);
                break;
            case 5:
            case 6:
            case 7:
                playerDeclarations.add(Declaration.QUINTE);
                break;
            case 8:
                playerDeclarations.add(Declaration.TIERCE);
                playerDeclarations.add(Declaration.QUINTE);
                break;
            default:
                break;
        }
    }

    private static int getHighestSequence(final List<Card> suitList) {
        int highestSequence = 1;
        int currentSequence = 1;

        for (int i = 1; i < suitList.size(); i++) {
            if (suitList.get(i).getRank().ordinal() ==
                    (suitList.get(i - 1).getRank().ordinal() + 1)) {

                ++currentSequence;
                if (currentSequence > highestSequence) {
                    highestSequence = currentSequence;
                }
            } else {
                currentSequence = 1;
            }
        }
        return highestSequence;
    }

    private static boolean containsBelote(final List<Card> cards) {
        return cards.stream()
                .anyMatch(card -> card.getRank().equals(Rank.QUEEN))
                && cards.stream()
                .anyMatch(card -> card.getRank().equals(Rank.KING));
    }

    public abstract Contract bid(final Contract highestContract);

    public abstract Card playCard(final Contract contract,
                                  final List<Card> cardsPlayedSoFar,
                                  final Suit dominantSuit);

    public abstract int chooseIndexToCutDeckAt(final int start, final int end);

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(name, player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    String handToString() {
        final String yourHandMessage = Message.get(Message.YOUR_HAND);
        return textAndCollectionToString(hand, yourHandMessage);
    }

    String contractsToString(final List<Contract> viableBids) {
        final String viableContractMessage = Message.get(
                Message.VIABLE_CONTRACT);
        return textAndCollectionToString(viableBids, viableContractMessage);
    }

    <T> String collectionToString(final Collection<T> collection) {
        final StringBuilder collectionToString = new StringBuilder();
        collection.forEach(c -> collectionToString.append(c).append(" "));
        return collectionToString.toString();
    }

    private <T> String textAndCollectionToString(final Collection<T> collection,
                                                 final String message) {

        String contentToString = "";
        contentToString += message + System.lineSeparator();
        contentToString += collectionToString(collection);
        contentToString += System.lineSeparator();
        return contentToString;
    }

    List<Contract> getValidContracts(final Contract highestContract) {
        final List<Contract> viableContracts = new ArrayList<>();

        viableContracts.add(Contract.PASS);
        viableContracts.addAll(Arrays.asList(Contract.values())
                .subList(highestContract.ordinal() + 1,
                        Contract.values().length));
        return viableContracts;
    }

    List<Card> getValidCards(final Contract contract,
                             final List<Card> cardsPlayedSoFar,
                             final Suit dominantSuit) {
        if (dominantSuit == null) {
            return hand;
        }

        final List<Card> cardsInHandFromDominantSuit =
                getCardsFromSuit(hand, dominantSuit)
                        .collect(Collectors.toList());

        if (cardsInHandFromDominantSuit.isEmpty()) {
            switch (contract) {
                case CLUBS:
                case DIAMONDS:
                case SPADES:
                case HEARTS:
                    final Suit trumpSuit = Suit.valueOf(contract.name());
                    final List<Card> cardsInHandFromTrumpSuit =
                            getCardsFromSuit(hand, trumpSuit)
                                    .collect(Collectors.toList());

                    if (cardsInHandFromTrumpSuit.isEmpty()) {
                        return hand;
                    }

                    final Optional<Card> highestCardFromTrumpSuitOnTable =
                            getCardsFromSuit(cardsPlayedSoFar, trumpSuit)
                                    .max(Comparator.comparing(card -> card
                                            .getValue(contract)));

                    if (!highestCardFromTrumpSuitOnTable.isPresent()) {
                        return cardsInHandFromTrumpSuit;
                    }

                    final List<Card> handCardsHigherThanTrumpSuitOnTable =
                            cardsInHandFromTrumpSuit.stream().filter(
                                    card -> card.getValue(contract)
                                            > highestCardFromTrumpSuitOnTable
                                            .get().getValue(contract))
                                    .collect(Collectors.toList());

                    if (handCardsHigherThanTrumpSuitOnTable.isEmpty()) {
                        return hand;
                    }
                    return handCardsHigherThanTrumpSuitOnTable;
                case NO_TRUMPS:
                case ALL_TRUMPS:
                    return hand;
                default:
                    break;
            }
        }

        final Optional<Card> maxCardOnTableFromDominantSuit =
                getCardsFromSuit(cardsPlayedSoFar, dominantSuit)
                        .max(Comparator.comparing(card -> card
                                .getValue(contract)));

        final List<Card> cardsInHandHigherThanDominantSuitOnTable =
                cardsInHandFromDominantSuit.stream().filter(
                        card -> card.getValue(contract)
                                > maxCardOnTableFromDominantSuit
                                .get().getValue(contract))
                        .collect(Collectors.toList());

        if (cardsInHandHigherThanDominantSuitOnTable.isEmpty()) {
            return cardsInHandFromDominantSuit;
        }

        return cardsInHandHigherThanDominantSuitOnTable;
    }

    private Stream<Card> getCardsFromSuit(final List<Card> cards,
                                          final Suit suit) {
        return cards.stream().filter(card -> card.getSuit().equals(suit));
    }
}
