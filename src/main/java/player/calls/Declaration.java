package player.calls;

public enum Declaration {
    TIERCE(20), QUARTE(50), QUINTE(100),
    CARRE_OF_NINES(150), CARRE_OF_JACKS(200),
    CARRE_OF_ACES(100), CARRE_OF_KINGS(100),
    CARRE_OF_QUEENS(100), CARRE_OF_TENS(100), BELOTE(20),
    NONE(0);

    private final int points;

    Declaration(final int points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }
}
