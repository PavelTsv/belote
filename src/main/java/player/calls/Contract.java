package player.calls;

public enum Contract {
    PASS, CLUBS, DIAMONDS, HEARTS, SPADES, NO_TRUMPS, ALL_TRUMPS;

    @Override
    public String toString() {
        return this.name();
    }
}
