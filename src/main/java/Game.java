import player.deck.exceptions.DeckIsEmptyException;
import player.utility.Message;
import player.utility.Pair;
import table.PlayTable;
import player.calls.Contract;
import player.calls.Declaration;
import player.deck.Card;
import player.deck.Deck;
import player.deck.Suit;
import player.Player;
import player.Team;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;

public class Game {
    private final Deck deck;
    private final Map<Player, Team> playerToTeam = new HashMap<>();
    private final Map<Team, Pair<Player, Player>> teamToPlayer =
            new HashMap<>();
    private final Queue<Player> playerTurns = new LinkedList<>();


    public Game(final Player player1, final Player player2,
                final Player player3, final Player player4) {

        final Team team1 = new Team(Message.get(Message.FIRST_TEAM_NAME));
        final Team team2 = new Team(Message.get(Message.SECOND_TEAM_NAME));
        playerToTeam.put(player1, team1);
        playerToTeam.put(player3, team1);
        playerToTeam.put(player2, team2);
        playerToTeam.put(player4, team2);
        teamToPlayer.put(team1, new Pair<>(player1, player3));
        teamToPlayer.put(team2, new Pair<>(player2, player4));

        playerTurns.addAll(playerToTeam.keySet());

        deck = new Deck();
    }

    public void start() {
        announceTeams();

        run();

        final Team winner = getWinner();
        announceWinner(winner);
    }

    private Team getWinner() {
        return teamToPlayer.keySet().stream()
                .max(Comparator.comparing(Team::getScore)).get();
    }

    private void run() {
        int round = 1;
        boolean isGameOver = false;
        boolean wasLastRoundValat = false;

        while (!isGameOver) {
            announceRoundAndTeamScore(round);

            final int firstDrawAmount = 3;
            final int secondDrawAmount = 2;
            System.out.println(Message.get(Message.DEAL_CARDS));
            dealCards(firstDrawAmount);
            dealCards(secondDrawAmount);

            System.out.println(Message.get(Message.BIDDING));
            final Pair<Team, Contract> teamContractPair = bid();
            System.out.println(Message.get(Message.CURRENT_CONTRACT) + " "
                    + teamContractPair.second);

            playContract(teamContractPair);

            isGameOver = checkEndCondition(wasLastRoundValat);

            final int minCutSizeIndex = 2;
            final int index = playerTurns.peek()
                    .chooseIndexToCutDeckAt(minCutSizeIndex,
                            deck.getSize() - minCutSizeIndex);
            deck.cutAt(index);
            playerTurns.add(playerTurns.remove());
            ++round;
        }
    }

    private void playContract(final Pair<Team, Contract> teamContractPair) {
        final int lastDrawAmount = 3;
        final Contract dominantContract = teamContractPair.second;

        if (dominantContract != Contract.PASS) {
            dealCards(lastDrawAmount);
            final PlayTable playTable = new PlayTable(dominantContract,
                    teamToPlayer.keySet());

            if (!dominantContract.equals(Contract.NO_TRUMPS)) {
                makeDeclarations(playTable);
            }

            playRound(teamContractPair, playTable);
        } else {
            playerTurns.forEach(player -> deck.addCards(player.dumpHand()));
        }
    }

    private void playRound(final Pair<Team, Contract> teamAndContract,
                           final PlayTable playTable) {
        final LinkedList<Player> trickTurnOrder = new LinkedList<>(playerTurns);

        playTricks(teamAndContract.second, playTable, trickTurnOrder);

        playTable.calculateFinalScore(teamAndContract.first);
        teamToPlayer.keySet().forEach(team ->
                team.addScore(playTable.getTeamScore(team)));
        deck.addCards(playTable.getCardPile());
    }

    private void playTricks(final Contract contract, final PlayTable playTable,
                            final LinkedList<Player> trickTurnOrder) {
        final int tricks = 8;
        final int finalTrick = tricks - 1;
        for (int trick = 0; trick < tricks; trick++) {
            final Suit dominantSuit = playCards(contract, playTable,
                    trickTurnOrder);

            final Player trickWinner = playTable.getTrickWinner(dominantSuit);
            final Team winningTeam = playerToTeam.get(trickWinner);
            playTable.addCardsToWinningTeamPile(winningTeam);

            announceTrickWinner(trickWinner);

            while (!trickTurnOrder.getFirst().equals(trickWinner)) {
                trickTurnOrder.addLast(trickTurnOrder.removeFirst());
            }
            if (trick == finalTrick) {
                final int lastTrickWinnerBonusPoints = 10;
                playTable.addScoreTo(winningTeam, lastTrickWinnerBonusPoints);
            }
        }
    }

    private Suit playCards(final Contract contract,
                           final PlayTable playTable,
                           final LinkedList<Player> trickTurnOrder) {

        Suit dominantSuit = null;
        for (int j = 0; j < trickTurnOrder.size(); j++) {
            final Player player = trickTurnOrder.get(j);
            final Card cardPlayed = player.playCard(contract,
                    playTable.getTrickCards(), dominantSuit);
            playTable.addCardToTrick(player, cardPlayed);
            System.out.println(player + " " + Message.get(Message.PLAYED) + " "
                    + cardPlayed);
            if (j == 0) {
                dominantSuit = cardPlayed.getSuit();
            }
        }
        System.out.println(Message.get(Message.LINE_SEPARATOR));
        return dominantSuit;
    }

    private void makeDeclarations(final PlayTable playTable) {
        for (final Player player : playerTurns) {
            final List<Declaration> playerDeclarations =
                    player.makeDeclarations();
            final Team playersTeam = playerToTeam.get(player);
            final StringBuilder declarations = new StringBuilder();
            for (final Declaration declaration : playerDeclarations) {
                declarations.append(declaration.name()).append(" ");
                playTable.addScoreTo(playersTeam, declaration.getPoints());
            }
            System.out.println(player.getName() + " "
                    + Message.get(Message.ANNOUNCED) + " " + declarations);
        }
    }

    private Pair<Team, Contract> bid() {
        final LinkedList<Player> biddingOrder = new LinkedList<>(playerTurns);
        int passesSoFar = 0;
        boolean isContractBound = false;
        Contract highestContract = Contract.PASS;
        Player highestBiddingPlayer = null;

        while (passesSoFar != 4 && !(isContractBound && passesSoFar == 3)) {
            final Player biddingPlayer = biddingOrder.remove();
            final Contract playerContract = biddingPlayer.bid(highestContract);
            System.out.println(biddingPlayer.getName() + " "
                    + Message.get(Message.BIDS) + " " + playerContract);
            if (playerContract.equals(Contract.PASS)) {
                ++passesSoFar;
            } else {
                highestContract = playerContract;
                highestBiddingPlayer = biddingPlayer;
                isContractBound = true;
                passesSoFar = 0;
            }
            biddingOrder.add(biddingPlayer);
        }
        return new Pair<>(playerToTeam.get(highestBiddingPlayer),
                highestContract);
    }

    private boolean checkEndCondition(final boolean wasLastRoundValat) {
        final int SCORE_REQUIRED_TO_WIN = 151;

        final Optional<Team> winningTeam = teamToPlayer.keySet().stream()
                .filter(team -> team.getScore() >= SCORE_REQUIRED_TO_WIN)
                .findAny();

        return winningTeam.isPresent() && !wasLastRoundValat;
    }

    private void dealCards(final int amount) {
        for (final Player player : playerTurns) {
            for (int i = 0; i < amount; i++) {
                try {
                    player.addToHand(deck.drawCard());
                } catch (DeckIsEmptyException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void announceTrickWinner(Player trickWinner) {
        System.out.println(trickWinner + " " + Message.get(Message.WON_TRICK));
        System.out.println(Message.get(Message.LINE_SEPARATOR));
    }

    private void announceWinner(final Team team) {
        System.out.println(Message.get(Message.LINE_SEPARATOR));
        System.out.println(Message.get(Message.WINNER) + " " + " " + team);
        System.out.println(Message.get(Message.LINE_SEPARATOR));
    }

    private void announceRoundAndTeamScore(final int round) {
        System.out.println(Message.get(Message.ROUND) + " " + round);
        teamToPlayer.forEach((team, playerPlayerPair) ->
                System.out.println(team + " " + Message.get(Message.SCORE) + " "
                        + team.getScore()));
        System.out.println();
    }

    private void announceTeams() {
        teamToPlayer.forEach((team, playerPlayerPair) ->
        {
            System.out.println(team + ":");
            System.out.println(playerPlayerPair.first);
            System.out.println(playerPlayerPair.second);

        });
        System.out.println();
    }
}
